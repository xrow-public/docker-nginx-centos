# docker-nginx-centos

This image is build to add default pre-start-scripts for the nginx s2i container.
Currently the pre-start-script needs to have a ENV variable defined called:

> LOCATION_URI

The script will create a location config for nginx that looks like this:

    location /${LOCATION_URI} {
         alias /opt/app-root/src/;
    }

To use this image execute like this:

    s2i build -e"LOCATION_URI=test/" . registry.gitlab.com/xrow-public/docker-nginx-centos myImage

